/******************************************************************************************************
Author  	: Wellington N. Rodrigues
Date    	: 22/01/2018
Purpose 	: Api Rest para buscar informações do Reimpressão Cupon para a aplicação de promoção
*******************************************************************************************************/
@RestResource(urlMapping='/ReimpressaoCupon/*')
global with sharing class ReimpressaoManager {
    @HttpGet
    global static List<TW1_Cupom__c> getReimpressaoCupon(){

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String accountId = req.params.get('IdCliente');
        String ShoppingId = req.params.get('IdShopping');
        
        try{
            list<TW1_Cupom__c> cupom = [SELECT 
                                            id, 
                                            name,
                                            TW1_Nome_do_Cliente__r.Name,
                                            TW1_Operador__r.Name,
                                            TW1_Data__c,
                                            TW1_Data_Reimpressao__c,
                                            TW1_Gerou_Reimpressao__c
                                        FROM 
                                        	TW1_Cupom__c 
                                        WHERE 
                                        	TW1_Nome_do_Cliente__c =:accountId
                                       ];
           	
            return cupom; 
        }catch(Exception e){
            return null;
        }
        
    }
}