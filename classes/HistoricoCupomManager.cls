/******************************************************************************************************
Author  	: Leonardo Façanha
Date    	: 22/12/2017
Purpose 	: Api Rest para buscar informações do Historico Cupon para a aplicação de promoção

-Campos Apagados
Senha Supervisor notas acima de (R$)	
Valor Min. de NTFS (R$)
Max. NTFS usadas de Troca
Max. NTFS mesma loja, Cliente e dia
Max. Brindes por Dia
Max. Brindes por Troca
Permitir Numeração Sequenciada	
Possui Limites de Brindes por NTFS	
Max. Brindes por NTFS	
Max. Participações
Concurso Cultural
Imprime Cupom Conc. Cult	
Possui Questionário
Imprime Cupom
Questionário Obrigatório
Transforma Dinheiro	
Imprime Regulamento - Botão	
Responde mais de uma vez
Direito para Aba de Baixo	
Imprime Reg. Automático	
Responde por Atendimento
Libera Aba Estorno	
Imprime Voucher	
Libera Aba Relacionamento

-Fazer verificação
Acumula Saldo ???????Verificar como faz???????
Max. Bônus por Cliente
Max. Brindes por Clientes
Bônus multiplica por	
Contabiliza Bônus


Troca por(Verificar para modificar a tela)
*******************************************************************************************************/
@RestResource(urlMapping='/HistoricoCupon/*')
global with sharing class HistoricoCupomManager {
    
    @HttpGet
    global static requestWrapper getHistoricoCuponPorId(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String accountId = req.params.get('IdCliente');
        String ShoppingId = req.params.get('IdShopping');
        
        HistoricoCupon historico = getHistoricoPorIdCliente(accountId,ShoppingId);
        
        requestWrapper acc = new requestWrapper();
        acc.historicoCupon = historico;
        
        return acc;
    }
    
    public static HistoricoCupon getHistoricoPorIdCliente(string accountId,string ShoppingId){
        //try{
        HistoricoCupon historicoCupom = new HistoricoCupon();
        //(public string NTrocados) CHEGANDO A SOMA DE QUANTOS CUPONS JA FORAM TROCADOS -
        date vaData = DateTime.now().date();  

        TW1_Parametros_Promocao__c Promocao = [Select Id,TW1_Data_Inicio__c,TW1_Data_Fim__c,TW1_Valor_Min_NTFS__c From TW1_Parametros_Promocao__c Where TW1_Shopping__c=:ShoppingId And TW1_Data_Fim__c >=:vaData Limit 1];
        List<TW1_Cupom__c> Cupons = [Select Id,Name From TW1_Cupom__c Where TW1_Nome_do_Cliente__c=:accountId And TW1_Parametro_Promocao__c=:Promocao.Id];	
        historicoCupom.NTrocados = Cupons.size();
        
        //(public string Saldo) CHEGANDO A SOMA Do Saldo(R$) e Numero de Cupon de direito
        List<NTFS__c> notasFiscais = [select Id From NTFS__c Where TW1_Cliente__c=:accountId And TW1_Shopping__c=:ShoppingId And TW1_Data_da_Nota__c >=:promocao.TW1_Data_Inicio__c And TW1_Data_da_Nota__c <=:promocao.TW1_Data_Fim__c];
        
        for(NTFS__c nota : notasFiscais)
        {
            //Regras do Promoção
            //Chegar ao valor maximo que ele tem direito de cupons

            If(nota.TW1_Forma_de_pagamento__c != 'Cartão Elo'){ 
                historicoCupom.Saldo = historicoCupom.Saldo + nota.TW1_Valor_RS__c;
            }
            else{
                historicoCupom.SaldoExtra = historicoCupom.SaldoExtra + nota.TW1_Valor_RS__c;
            }
            
        }
        
        //Dividir o Saldo e Saldo extra pelo valor configurado no paramentro da promoção para corte de cupons.
        Decimal NumeroDireto = historicoCupom.Saldo / promocao.TW1_Valor_Min_NTFS__c;
        Decimal NumeroDiretoExtra = historicoCupom.SaldoExtra / promocao.TW1_Valor_Min_NTFS__c;
		
        //Subtrair pelo numero de cupons que ele já trocou
        historicoCupom.NDireito = NumeroDireto + (NumeroDiretoExtra * 2.0);
        
        return historicoCupom;
        
        //} catch (Exception ce) {
        //return null;
        //}  
        
    }
    
    global class requestWrapper 
    {
        public HistoricoCupon historicoCupon;
    }
    
    global class HistoricoCupon 
    {
        public decimal Saldo;
        public decimal SaldoExtra;
        public Integer NTrocados;
        public decimal NDireito;
    }
}