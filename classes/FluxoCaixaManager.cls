/******************************************************************************************************
Author  	: Leonardo Façanha
Date    	: 05/01/2017
Purpose 	: Api Rest para buscar/Inserir informações do fluxo de Caixa para a aplicação de promoção
*******************************************************************************************************/
@RestResource(urlMapping='/FluxoCaixa/*')
global with sharing class FluxoCaixaManager {
    
    
    @HttpGet
    global static string getAberturaCaixaPorId(){
        RestRequest req = RestContext.request;
        
        String vaTerminalId = req.params.get('IdTerminal');
        String vaOperadorId = req.params.get('IdOperador');
        String vaValor = req.params.get('Valor');
        
        If((vaTerminalId != null && vaTerminalId != '' && vaTerminalId != 'null') && (vaOperadorId != null && vaOperadorId != '' && vaOperadorId != 'null') && (vaValor != null && vaValor != '' && vaValor != 'null')){
            
            TW1_Terminal__c terminal = [Select Id,Name From TW1_Terminal__c where Id=:vaTerminalId];
            terminal.TW1_Operador__c = vaOperadorId;
            Update terminal;
            
            TW1_Caixa__c caixa = new TW1_Caixa__c();
            caixa.TW1_Operador__c = vaOperadorId;
            caixa.Name = terminal.Name;
            insert caixa;
            
            TW1_Extrato__c extrato = new TW1_Extrato__c();
            extrato.TW1_Tipo__c = 'ValorAbertura';
            extrato.TW1_Valor__c = Decimal.valueOf(vaValor);
            extrato.TW1_Data__c = datetime.now();
            extrato.TW1_Caixa__c = caixa.Id;
            
            insert extrato;
            
            return 'Caixa aberto com sucesso!';
        }
        
        return 'Erro ao abrir caixa';         
    }    
}