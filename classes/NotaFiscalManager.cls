/******************************************************************************************************
Author  	: Leonardo Façanha
Date    	: 22/11/2017
Purpose 	: Api Rest para buscar informações do NotaFiscal para a aplicação de promoção
*******************************************************************************************************/
@RestResource(urlMapping='/NotaFiscal/*')
global with sharing class NotaFiscalManager {
    
    @HttpGet
    global static List<NTFS__c> getNotaFiscalPorId(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        List<NTFS__c> listaNTFSPorId = new List<NTFS__c>();
        
        String NTFSId = req.params.get('Id');
        string NumeroNTFS = req.params.get('NumeroNTFS');
        string IdCliente = req.params.get('IdCliente');
        
        If(NTFSId != null && NTFSId != '' && NTFSId != 'null'){
            
            try{
                
                listaNTFSPorId = 
                    [ 
                        SELECT Id,
                        Name,
                        TW1_Cliente__c,
                        TW1_Data_da_Nota__c,
                        TW1_Forma_de_pagamento__c,
                        TW1_Loja__c,
                        TW1_Valor_RS__c 
                        FROM NTFS__c 
                        WHERE Id=:NTFSId LIMIT 1
                    ];
                
                return listaNTFSPorId; 
                
            }catch(exception e){
                return null;
            }  
        }
        
        If(NumeroNTFS != null && NumeroNTFS != '' && NumeroNTFS != 'null' || IdCliente != null && IdCliente != '' && IdCliente != 'null'){
            try{
                
                list<NTFS__c> listaNTFS =
                    [ 
                        SELECT Id,
                        Name,
                        TW1_Cliente__r.Name,
                        TW1_Data_da_Nota__c,
                        TW1_Forma_de_pagamento__c,
                        TW1_Loja__r.Name,
                        TW1_Valor_RS__c 
                        FROM NTFS__c
                        WHERE TW1_Cliente__c=:IdCliente and Name Like:'%'+NumeroNTFS+'%'
                    ];
                                
                return listaNTFS; 

            }catch(exception e){
                return null;
            }  
        }

        return listaNTFSPorId; 
    }
    
    @HttpPost
    global static NTFS__c UpsertNotaFiscal(requestWrapper RequestWrapper){
        try{          
            NTFS__c ntfs = MapNTFS(RequestWrapper.notaFiscal);
            
            if(ntfs.Id != null){
                update ntfs; 
                return ntfs;
            }else{
                
                List<NTFS__c> listaNTFSPorId = 
                    [ 
                        SELECT Id,
                        Name,
                        TW1_Cliente__c,
                        TW1_Data_da_Nota__c,
                        TW1_Forma_de_pagamento__c,
                        TW1_Loja__c,
                        TW1_Valor_RS__c 
                        FROM NTFS__c 
                        WHERE Id=:ntfs.Id LIMIT 1
                    ];
                
                if(listaNTFSPorId.size() <= 0){
                    insert ntfs;
                    return ntfs;
                }else{
                    return new NTFS__c();
                }
                
            }
        } catch (Exception ce) {
            return new NTFS__c();
        }  
    }
    
    global class requestWrapper 
    {
        public NotaFiscal notaFiscal;
    }
    
    global class NotaFiscal 
    {
        public string Id;
        public string NumeroNota;
        public string IdLoja;
        public string IdShopping;
        public string FormaPagamento;
        public string IdCliente;
        public Date DataNota;
        public string Valor; 
    }
    
    Private static NTFS__c MapNTFS(NotaFiscal notaFiscal){
        NTFS__c ntfs = new NTFS__c();
        
        ntfs.Id = notaFiscal.Id;
        ntfs.Name = notaFiscal.NumeroNota;
        ntfs.TW1_Cliente__c = notaFiscal.IdCliente;
        ntfs.TW1_Data_da_Nota__c = notaFiscal.DataNota;
        ntfs.TW1_Shopping__c = notaFiscal.IdShopping;
        ntfs.TW1_Forma_de_pagamento__c = notaFiscal.FormaPagamento;
        ntfs.TW1_Loja__c = notaFiscal.IdLoja;
        ntfs.TW1_Valor_RS__c = decimal.valueOf(notaFiscal.Valor);
        
        return ntfs;
    } 
}