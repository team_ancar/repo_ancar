@isTest
public class ValidacaoCpfCnpjAnnotation_test {
	
    @isTest static void test_ValidacaoCpfCnpjAnnotation_CPF(){
        
        Account acct = new Account();
        acct.Name='Test de validação cpf e cnpj';
        acct.TW1_CPF__c='747.457.105-69';
        acct.TW1_CNPJ__c='27.364.928/0001-01';
        insert acct;

        list<String> vaact = new list<String>();
        vaact.add('957.073.210-55');
        vaact.add('946.832.280-76');
        vaact.add('314.117.620-53');
        
        Test.startTest();
        	ValidacaoCpfCnpjAnnotation.validar_cpf_cnpj(vaact);
        Test.stopTest();

    }
    @isTest static void test_ValidacaoCpfCnpjAnnotation01_CNPJ(){
        
        Account acct = new Account();
        acct.Name='Test de validação cpf e cnpj';
        acct.TW1_CPF__c='747.457.105-69';
        acct.TW1_CNPJ__c='89.911.379/0001-03';
        insert acct;
		
        
        list<String> vaact = new list<String>();
        vaact.add('39.945.925/0001-40');
        vaact.add('47.055.177/0001-59');
        vaact.add('89.911.379/0001-03');
        
        Test.startTest();
        	ValidacaoCpfCnpjAnnotation.validar_cpf_cnpj(vaact);
        Test.stopTest();

    }
   
    
}