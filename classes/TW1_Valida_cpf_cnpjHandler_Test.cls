@isTest
public class TW1_Valida_cpf_cnpjHandler_Test {
	
    @isTest static void test_validar_cpf_cnpj_insert1(){
        
        Account acct = new Account();
        acct.Name='Test de validação cpf e cnpj';
        acct.TW1_CPF__c='747.457.105-69';
        acct.TW1_CNPJ__c='27.364.928/0001-01';
        
        Test.startTest();
        Database.SaveResult result1 = Database.insert(acct,false);
        Test.stopTest();
        
        System.assert(result1.isSuccess());
    }
    
    @isTest static void test_validar_cpf_cnpj_Update(){

        Account acct = new Account();
        acct.Name='Test de validação cpf e cnpj';
        acct.TW1_CPF__c='328.164.429-50';
        acct.TW1_CNPJ__c='58.927.175/0001-29';

        Insert acct;
        
        Account n = [select id,TW1_CPF__c,TW1_CNPJ__c from Account where id =:acct.Id  limit 1];
        n.TW1_CPF__c = '133.691.447-51';
        n.TW1_CNPJ__c = '46.185.012/0001-39';
        
        Test.startTest();
        	Database.SaveResult result1 = Database.update(n,false);
        Test.stopTest();
        
        System.assert(result1.isSuccess());

    }
    

}