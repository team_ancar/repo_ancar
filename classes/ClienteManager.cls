/******************************************************************************************************
Author  	: Leonardo Façanha
Date    	: 22/11/2017
Atualizada 	: 15/12/2017 - Wellingto N. Rodrigues
Purpose 	: Api Rest para buscar informações do cliente para a aplicação de promoção
*******************************************************************************************************/
@RestResource(urlMapping='/Cliente/*')
global with sharing class ClienteManager {
    
    @HttpGet
    global static List<Account> getClientePorId(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String accountId = req.params.get('Id');
        
        If(accountId != null && accountId != '' && accountId != 'null'){
            
            try{
                
                List<Account> listaClientePorId = 
                    [ 
                        SELECT 
                        Id,
                        FirstName,
                        LastName,
                        TW1_Av_Rua__c,
                        TW1_Numero__c,
                        TW1_Bairro__c,
                        CEP__c,
                        TW1_Complemento__c,
                        TW1_Estado_civil__c,
                        TW1_Sexo__c,
                        TW1_Cidade__c,
                        TW1_UF_de_emissao__c,
                        TW1_CPF__c,
                        TW1_Identidade__c,
                        TW1_Email__c,
                        TW1_Permite_SMS__c,
                        TW1_Data_Nascimento__c,
                        DDD__c,
                        TW1_DDI__c,
                        Phone,
                        TW1_EmailCB__c,
                        TW1_TelefoneCB__c,
                        TW1_Correspondencia__c
                        FROM Account 
                        WHERE 
                        Id=:AccountId LIMIT 1
                    ];
                return listaClientePorId; 
                
            }catch(exception e){
                return null;
            }
        }
        else{
            
            String nome = req.params.get('nome');
            String cpf = req.params.get('cpf');
            
            List<Account> listaAccount;
            
            if(nome == '' && cpf == ''){
                return listaAccount;
            }
            else{
                
                listaAccount = [ 
                    SELECT Id, Name, TW1_CPF__c, TW1_Data_Nascimento__c  
                    FROM Account 
                    WHERE recordtype.id='0122F0000004X8uQAE'And Name Like:'%'+nome+'%' And TW1_CPF__c Like:'%'+cpf+'%'
                ];
                
            }        
            return listaAccount;
        }
    }
    
    
    
    @HttpPost
    global static Account UpsertCliente(requestWrapper RequestWrapper){
        try{          
            Account account = MapAccount(RequestWrapper.cliente);
            
            if(account.Id != null){
                update account; 
                return account;
            }else{
                insert account;
                return account;
            }
        } catch (Exception ce) {
            return new account();
        }  
    }
    
    global class requestWrapper 
    {
        public Cliente cliente;
    }
    
    global class Cliente 
    {
        public string Id;
        public string Name;
        public string Sobrenome;
        public string Av_Rua;
        public decimal Numero;
        public string Bairro;
        public decimal CEP;
        public string Complemento;
        public string Estado_civil;
        public string Sexo;
        public string Cidade;
        public string UF_de_emissao;
        public string CPF;
        public string Identidade;
        public string Email;
        public Boolean Permite_SMS;
        public date Data_Nascimento;
        public decimal DDD;
        public decimal DDI;
        public string Phone;
        public Boolean EmailCB;
        public Boolean TelefoneCB;
        public Boolean Correspondencia;
    }
    
    Private static Account MapAccount(Cliente cliente){
        Account account = new Account();
        
        account.Id = cliente.Id;
        account.FirstName = cliente.Name;
        account.LastName = cliente.Sobrenome;
        account.TW1_Av_Rua__c = cliente.Av_Rua;
        account.TW1_Numero__c = cliente.Numero;
        account.TW1_Bairro__c = cliente.Bairro;
        account.CEP__c = cliente.CEP;
        account.TW1_Complemento__c = cliente.Complemento;
        account.TW1_Estado_civil__c = cliente.Estado_civil;
        account.TW1_Sexo__c = cliente.Sexo;
        account.TW1_Cidade__c = cliente.Cidade;
        account.TW1_UF_de_emissao__c = cliente.UF_de_emissao;
        account.TW1_CPF__c = cliente.CPF;
        account.TW1_Identidade__c = cliente.Identidade;
        account.TW1_Email__c = cliente.Email;
        account.TW1_Permite_SMS__c = cliente.Permite_SMS;
        account.TW1_Data_Nascimento__c = cliente.Data_Nascimento;
        account.DDD__c = cliente.DDD;
        account.TW1_DDI__c = cliente.DDI;
        account.Phone = cliente.Phone;
        account.TW1_TelefoneCB__c = cliente.EmailCB;
        account.TW1_EmailCB__c = cliente.TelefoneCB;
        account.TW1_Correspondencia__c = cliente.Correspondencia;
        account.RecordTypeId = '0122F0000004X8uQAE';
        
        return account;
    } 
}