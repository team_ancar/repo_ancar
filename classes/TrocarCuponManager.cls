/******************************************************************************************************
Author  	: Leonardo Façanha
Date    	: 22/12/2017
Purpose 	: Api Rest para buscar informações do Trocar Cupon para a aplicação de promoção
*******************************************************************************************************/
@RestResource(urlMapping='/TrocarCupon/*')
global with sharing class TrocarCuponManager {
	
    @HttpGet
    global static requestWrapper getTrocarCuponPorId(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
      
        String accountId = req.params.get('Id');
        Integer Quantidade = Integer.valueOf(req.params.get('QuantidadeCupons'));
        String Shopping = req.params.get('IDShopping');
        String IdOperador = req.params.get('IDOperador');
		
        requestWrapper acc = new requestWrapper();
        acc.cupon = new List<TW1_Cupom__c>();
        
        TW1_Parametros_Promocao__c Promocao = [Select Id,TW1_Cert_de_Aut__c,TW1_Pergunta__c From TW1_Parametros_Promocao__c Where TW1_Shopping__c=:Shopping];
        List<TW1_Cupom__c> Listcup = new List<TW1_Cupom__c>();
        
        for(Integer i = 0; i <= Quantidade; i++)
        {
            TW1_Cupom__c cup = new TW1_Cupom__c();
            
              cup.TW1_Data__c = DateTime.now().date();
              cup.TW1_Nome_do_Cliente__c = accountId;  
           	  cup.TW1_Certificado_Autenticao_Promocao__c = 	Promocao.TW1_Cert_de_Aut__c;
              cup.TW1_Pergunta__c = Promocao.TW1_Pergunta__c;
              cup.TW1_Operador__c = IdOperador;
              cup.TW1_Parametro_Promocao__c = Promocao.Id;
              
              Listcup.add(cup);             
        }  
        try{
        insert Listcup;
        }
        catch (Exception ex) {
               return new requestWrapper();
           }  
        
         acc.cupon = Listcup;
        
         return acc;
    }
    
    global class requestWrapper 
    {
		public List<TW1_Cupom__c> cupon;
    }
}