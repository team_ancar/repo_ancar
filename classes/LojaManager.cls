/******************************************************************************************************
Author  	: Leonardo Façanha
Date    	: 22/12/2017
Purpose 	: Api Rest para buscar informações do Loja para a aplicação de promoção
*******************************************************************************************************/
@RestResource(urlMapping='/Loja/*')
global with sharing class LojaManager {
		
    @HttpGet
    global static List<Account> getLojaPorId(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
      
        String LojaId = req.params.get('Id');
        
        If(LojaId != null && LojaId != '' && LojaId != 'null'){
            
        try{
                        
             List<Account> listaLojaPorId = 
                [ 
                    SELECT 
                    Id,
                    Name,
                    LUC__c,
                    TW1_CNPJ__c,
                    TW1_Nome_fantasia__c,
                    TW1_Razao_Social__c
                    FROM Account 
                    WHERE 
                    Id=:LojaId LIMIT 1
                ];
            return listaLojaPorId; 
            
        }catch(exception e){
            return null;
        }
        }
        else{

            String Luc = req.params.get('Luc');
            String Cnpj = req.params.get('Cnpj');
            String Nome = req.params.get('Nome');
            String Razao = req.params.get('Razao');
            
            List<Account> listaAccount;
        
        if((Luc == '' && Cnpj == '' && Nome == '' && Razao == '') || (Luc == null && Cnpj == null && Nome == null && Razao == null)){
            return listaAccount;
        }
            else{
           	              
                    listaAccount = [ 
                SELECT Id,
                    Name,
                    LUC__c,
                    TW1_CNPJ__c,
                    TW1_Nome_fantasia__c,
                    TW1_Razao_Social__c
                FROM Account 
               WHERE LUC__c Like:Luc+'%' or TW1_Nome_fantasia__c Like:Nome+'%' or TW1_CNPJ__c Like:Cnpj+'%' or TW1_Razao_Social__c Like:Razao+'%' limit 1
            ];
                return listaAccount;
          }
        }        
     }
}